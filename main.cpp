
#include <iostream>
#include <Windows.h>
#include <WinUser.h>
//#include <WinBase.h>
#include <tchar.h>
#include <WinUser.h>
#include <string.h>

#pragma comment(lib, "User32.lib")
#pragma comment(lib, "Kernel32.Lib")

#define MsgBoxW(s) MessageBoxW(NULL,TEXT(s),TEXT("消息"),MB_OK)
#define MsgBoxA(s) MessageBoxA(NULL,s,"消息",MB_OK)

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int ncmdShow)
{

    static TCHAR szAppName[]=TEXT("Windows");
    HWND hwnd;
    MSG msg;
    WNDCLASSEX myclass;

    ZeroMemory(&myclass,sizeof(WNDCLASSEX));

    myclass.hCursor=LoadCursor(0,IDC_ARROW);
    myclass.hInstance=hInstance;
    myclass.cbSize=sizeof(WNDCLASSEX);
    myclass.style=CS_HREDRAW | CS_VREDRAW;
    myclass.lpfnWndProc=WndProc;
    myclass.hbrBackground=(HBRUSH)(COLOR_WINDOW + 1);
    myclass.lpszClassName=TEXT("myclass");
    /*
    myclass.hIcon=NULL;
    myclass.lpszMenuName=NULL;
    myclass.hIconSm=NULL;
    myclass.cbClsExtra=NULL;
    myclass.cbWndExtra=NULL;
    */

    if(!RegisterClassEx(&myclass)) MsgBoxW("注册窗口失败");

    hwnd=CreateWindowEx(
                WS_EX_CLIENTEDGE,
                TEXT("myclass"),
                TEXT("我的窗口"),
                WS_OVERLAPPEDWINDOW,
                100,100,600,400,
                NULL,NULL,hInstance,NULL);

    ShowWindow(hwnd,SW_SHOWMAXIMIZED);
    UpdateWindow(hwnd);

    while(1)
    {
        if(0==GetMessage(&msg,NULL,0,0)) break;
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    };
    MsgBoxW("退出");

}


LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
    case WM_CREATE:

        //Put your code here
        return 0;

    case WM_PAINT:
        //Put your code here
        return 0;

    case WM_DESTROY:
        DestroyWindow(hwnd);
        PostQuitMessage(0);
        return 0;
    }
    return DefWindowProc(hwnd,message,wParam,lParam);
}
